<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Rent Houses</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>


</head>
<body>
<nav class="blue-grey lighten-5" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="<c:url value="/"/> " class="brand-logo">Main</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="<c:url value="/addRentalUnit"/>">AddRentalUnit</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li><a href="<c:url value="/addRentalUnit"/>">AddRentalUnit</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>


<jsp:doBody/>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="resources/js/materialize.js"></script>
<script src="resources/js/init.js"></script>

</body>
</html>