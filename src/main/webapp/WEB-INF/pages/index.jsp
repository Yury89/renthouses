<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<t:template>

    <div class="row">
        <c:forEach items="${mainRentalUnits}" var="rent">
            <div class=" card col s6 m12 l4 card-panel hoverable">
                <div class="card-image">

                    <img class="activator cursor-pointer"
                         src="<c:url  value="/uploaded_images/${rent.images.get(0).id}.jpg"/>">

                    <span class="card-title">${rent.title}</span>

                </div>
                <div class="card-content">
                    <p>${rent.description}</p>

                </div>
                <div class="card-action">
                    <a href="<c:url value="/rentalunit?id=${rent.id}"/>">More</a>
                </div>
                <div class="card-reveal accent-2">
                <span class="card-title grey-text text-darken-4 "><a href="<c:url value="/rentalunit?id=${rent.id}"/>">${rent.title}</a><i
                        class="material-icons right">close</i></span>

                    <div class="row">
                        <c:if test="${rent.price != null}">
                            <div class="card ">
                                <div class="card-content black-text">
                                    <span class="card-title">Price List</span>
                                    <p> <c:if test="${rent.price.month != null}">
                                        For Month ${rent.price.month}
                                    </c:if>
                                        <br>
                                        <c:if test="${rent.price.week != null}">
                                            For Week ${rent.price.week}
                                        </c:if>
                                        <br>
                                        <c:if test="${rent.price.day != null}">
                                            For Day ${rent.price.day}
                                        </c:if>
                                        <br>
                                    </p>
                                </div>
                            </div>
                        </c:if>

                        <c:if test="${rent.address != null}">
                            <div class="card ">
                                <div class="card-content black-text">
                                    <span class="card-title">Address</span>
                                    <p>${rent.address.country} ${rent.address.region} ${rent.address.city}
                                            ${rent.address.street} ${rent.address.homeNumber} ${rent.address.room}</p>
                                </div>
                            </div>
                        </c:if>


                            <div class="card ">
                                <div class="card-content black-text">
                                    <span class="card-title">Polices</span>
                                    <p><c:forEach items="${rent.polices}" var="polic">
                                        ${polic.name}: ${polic.value} <br>
                                    </c:forEach>
                                    </p>
                                </div>
                            </div>

                    </div>

                </div>
            </div>
        </c:forEach>
    </div>

</t:template>