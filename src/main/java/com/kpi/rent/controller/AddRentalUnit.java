package com.kpi.rent.controller;

import com.kpi.rent.entities.RentalUnit;
import com.kpi.rent.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/addRentalUnit")
public class AddRentalUnit  {




    @Autowired
    private RentalUnitService rentalUnitService;


    @RequestMapping(method = RequestMethod.GET)
    public String addRentalUnitForm(Model model) {
        model.addAttribute("rentalForm",new RentalUnit());
        model.addAttribute("allRentalUnits",rentalUnitService.getAllRentalUnit());
        return "addRentalUnit";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String saveRentalUnitForm(@ModelAttribute("rentalForm") RentalUnit rentalUnit, @RequestParam(name = "nameForRentalUnit")Long idForRentalUnit){
        rentalUnitService.saveImageAndRentalUnit(rentalUnit,idForRentalUnit);

        return "redirect:/";
    }



}
