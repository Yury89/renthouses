package com.kpi.rent.controller;

import com.kpi.rent.entities.RentalUnit;
import com.kpi.rent.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.LinkedHashSet;

@Controller
@RequestMapping(value = "/rentalunit")
public class RentalUnitsController {

    @Autowired
    private RentalUnitService rentalUnitService;

    @RequestMapping
    public String getRentalUnitsById(Model model, @RequestParam(value = "id") int id) {

        LinkedHashSet<RentalUnit> parentRentalUnitsById = rentalUnitService.getParentRentalUnitsById(id);
        model.addAttribute("child", rentalUnitService.getChildrenRentalUnitsById(id));
        model.addAttribute("parent", parentRentalUnitsById);
        model.addAttribute("current", rentalUnitService.getById(id));
        model.addAttribute("amenities",rentalUnitService.getAllAmenities(parentRentalUnitsById));
        model.addAttribute("polices",rentalUnitService.getAllPolices(parentRentalUnitsById));

        return "rentalUnits";

    }
}
