package com.kpi.rent.service;

import com.kpi.rent.entities.Image;
import com.kpi.rent.entities.RentalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;

@Service
public class ImageService {

    @Autowired
    private ServletContext servletContext;

    public void uploadImage(RentalUnit rentalUnit) {

        try {
            for (Image image : rentalUnit.getImages()) {
                String path = servletContext.getRealPath("/uploaded_images/") + image.getId() + ".jpg";
                image.getMultipartFile().transferTo(new File(path));
                image.setRentalUnit(rentalUnit);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
