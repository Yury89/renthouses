package com.kpi.rent.service;

import com.kpi.rent.entities.Amenities;
import com.kpi.rent.entities.Polices;
import com.kpi.rent.entities.RentalUnit;
import com.kpi.rent.repository.RentalUnitRepository;

import java.util.LinkedHashSet;

public interface RentalUnitService extends RentalUnitRepository {

    LinkedHashSet<RentalUnit> getParentRentalUnitsById(long id);

    LinkedHashSet<Amenities> getAllAmenities(LinkedHashSet<RentalUnit> rentalUnits);

    LinkedHashSet<Polices> getAllPolices(LinkedHashSet<RentalUnit> rentalUnits);

    void saveImageAndRentalUnit(RentalUnit rentalUnit, Long id);


}
