package com.kpi.rent.service;

import com.kpi.rent.entities.Amenities;
import com.kpi.rent.entities.Image;
import com.kpi.rent.entities.Polices;
import com.kpi.rent.entities.RentalUnit;
import com.kpi.rent.repository.RentalUnitRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedHashSet;

@Service
public class RentalUnitServiceImpl extends RentalUnitRepositoryImpl implements RentalUnitService {


    @Autowired
    private ImageService imageService;


    @Override
    public LinkedHashSet<RentalUnit> getParentRentalUnitsById(long id) {
        RentalUnit parentRentalUnitById = getById(id);
        LinkedHashSet<RentalUnit> rentalUnits = new LinkedHashSet<>();
        while (parentRentalUnitById.getRentalUnit() != null) {
            parentRentalUnitById = parentRentalUnitById.getRentalUnit();
            rentalUnits.add(parentRentalUnitById);
        }
        return rentalUnits;
    }

    @Override
    public LinkedHashSet<Amenities> getAllAmenities(LinkedHashSet<RentalUnit> rentalUnits) {
        Iterator<RentalUnit> iterator = rentalUnits.iterator();
        LinkedHashSet<Amenities> amenities = new LinkedHashSet<>();
        while (iterator.hasNext())
            amenities.addAll(iterator.next().getAmenities());
        return amenities;
    }

    @Override
    public LinkedHashSet<Polices> getAllPolices(LinkedHashSet<RentalUnit> rentalUnits) {
        Iterator<RentalUnit> iterator = rentalUnits.iterator();
        LinkedHashSet<Polices> amenities = new LinkedHashSet<>();

        while (iterator.hasNext())
            amenities.addAll(iterator.next().getPolices());
        return amenities;
    }

    //Amenities,Polices,Images, Price, Adress
    @Override
    public void saveImageAndRentalUnit(RentalUnit rentalUnit, Long id) {
        if (id != -1)
            rentalUnit.setRentalUnit(super.getById(id));
        addIdToProperties(rentalUnit);
        save(rentalUnit);
        imageService.uploadImage(rentalUnit);

    }

    private void addIdToProperties(RentalUnit rentalUnit) {
        for (Amenities amenities : rentalUnit.getAmenities()) {
            amenities.setRentalUnit(rentalUnit);
        }
        for (Polices polices : rentalUnit.getPolices()) {
            polices.setRentalUnit(rentalUnit);
        }
        for (Image image : rentalUnit.getImages()) {
            image.setRentalUnit(rentalUnit);
        }

        for (Image image : rentalUnit.getImages()) {
            image.setRentalUnit(rentalUnit);
        }
        rentalUnit.getPrice().setRentalUnit(rentalUnit);
        rentalUnit.getAddress().setRentalUnit(rentalUnit);


    }

}
