package com.kpi.rent.entities;

import javax.persistence.*;

@Entity
@Table(name = "AMENITIES")
public class Amenities extends BaseEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rental")
    private RentalUnit rentalUnit;

    public Amenities(String title, String description, RentalUnit rentalUnit) {

        this.title = title;
        this.description = description;
        this.rentalUnit = rentalUnit;
    }

    public Amenities() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }
}
