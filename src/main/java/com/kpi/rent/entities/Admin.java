package com.kpi.rent.entities;

import javax.persistence.*;

@Entity
@Table(name = "ADMIN")
public class Admin extends BaseEntity{



    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;



    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Admin() {

    }

    public Admin(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
