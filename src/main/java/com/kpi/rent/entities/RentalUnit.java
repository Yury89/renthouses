package com.kpi.rent.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "RENTAL_UNIT")
@NamedQueries({
        @NamedQuery(name = "getRentalUnitById", query = "from RentalUnit r where r.id = :id"),
        @NamedQuery(name = "getMainRentalUnits", query = "from RentalUnit r where r.rentalUnit = null"),
        @NamedQuery(name = "getChildrenRentalUnitsById", query = "from RentalUnit r where r.rentalUnit.id = :id"),
        @NamedQuery(name = "getAllRentalUnit", query = "select r  from RentalUnit r"),

}
)
public class RentalUnit extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "rentalUnit",cascade = CascadeType.ALL)
    private Price price;

    @OneToOne(mappedBy = "rentalUnit",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Address address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rentalUnit",cascade = CascadeType.ALL)
    private List<Amenities> amenities;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rentalUnit",cascade = CascadeType.ALL)
    private List<Image> images;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rentalUnit",cascade = CascadeType.ALL)
    private List<Polices> polices;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_parent", referencedColumnName = "id")
    private RentalUnit rentalUnit;



    public List<Polices> getPolices() {
        return polices;
    }

    public void setPolices(List<Polices> properties) {
        this.polices = properties;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }


    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public RentalUnit(String name, String title, String description, List<Image> images, List<Polices> polices, RentalUnit rentalUnit) {
        this.name = name;
        this.title = title;
        this.description = description;
        this.images = images;
        this.polices = polices;
        this.rentalUnit = rentalUnit;

    }

    public RentalUnit() {
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Amenities> getAmenities() {
        return amenities;
    }


    public void setAmenities(List<Amenities> amenities) {
        this.amenities = amenities;
    }


    public String getName() {
        return name;
    }

    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit id_connect) {
        this.rentalUnit = id_connect;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return super.getId().hashCode();
    }

    @Override
    public String toString() {
        return super.getId() + " " + name + " " + title + " " + description;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RentalUnit && this.getId().equals(((RentalUnit) obj).getId());
    }


}