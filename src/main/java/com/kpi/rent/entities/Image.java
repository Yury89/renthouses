package com.kpi.rent.entities;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
@Table(name = "IMAGE")
public class Image extends BaseEntity {


    @Transient
    private MultipartFile multipartFile;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "image_rental")
    private RentalUnit rentalUnit;


    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public Image() {

    }

    public Image(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
