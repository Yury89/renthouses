package com.kpi.rent.entities;

import javax.persistence.*;

@Entity
@Table(name = "POLICES")
public class Polices extends BaseEntity {


    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rental")
    private RentalUnit rentalUnit;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public Polices() {

    }

    public Polices(String name, String value, RentalUnit rentalUnit) {

        this.name = name;
        this.value = value;
        this.rentalUnit = rentalUnit;
    }
}
