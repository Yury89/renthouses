package com.kpi.rent.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PRICE")
public class Price  extends BaseEntity{



    @Column(name = "day")
    private BigDecimal day;

    @Column(name = "week")
    private BigDecimal week;

    @Column(name = "month")
    private BigDecimal month;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rental_unit")
    private RentalUnit rentalUnit;

    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }



    public BigDecimal getDay() {
        return day;
    }

    public void setDay(BigDecimal day) {
        this.day = day;
    }

    public BigDecimal getWeek() {
        return week;
    }

    public void setWeek(BigDecimal week) {
        this.week = week;
    }

    public BigDecimal getMonth() {
        return month;
    }

    public void setMonth(BigDecimal month) {
        this.month = month;
    }




}
