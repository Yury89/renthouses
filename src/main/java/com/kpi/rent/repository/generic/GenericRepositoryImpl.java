package com.kpi.rent.repository.generic;

import com.kpi.rent.entities.BaseEntity;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



public class GenericRepositoryImpl<T extends BaseEntity> {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected List<T> findByNamedQuery(String queryString) {
        // return entityManager.createNamedQuery(queryString).getResultList();
        return sessionFactory.openSession().getNamedQuery(queryString).list();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected List<T> findByNamedQueryParameter(String queryString, String parameterName, Object parameter) {
//        return entityManager.createNamedQuery(queryString).
//                setParameter(parameterName, parameter).getResultList();
        return sessionFactory.openSession().getNamedQuery(queryString).setParameter(parameterName, parameter).list();
    }

    /**
     * Convenience method for obtaining the list of objects using the named query and its parameters.
     */
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected List<T> findByNamedQueryParameters(String queryString, String[] parameterNames, Object[] parameters) {
//        Query query = entityManager.createNamedQuery(queryString);
//
//        for(int i=0;i < parameterNames.length ; i++ ) {
//            query.setParameter(parameterNames[i], parameters[i]).getResultList();
//        }
//        return query.getResultList();
        Query query = sessionFactory.openSession().getNamedQuery(queryString);
        for (int i = 0; i < parameterNames.length; i++) {
            query.setParameter(parameterNames[i], parameters[i]).list();
        }
        return query.list();
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public void save(T t){
        sessionFactory.openSession().save(t);
    }


}