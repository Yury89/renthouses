package com.kpi.rent.repository;


import com.kpi.rent.entities.RentalUnit;
import com.kpi.rent.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class RentalUnitRepositoryImpl extends GenericRepositoryImpl<RentalUnit> implements RentalUnitRepository{



    @Override
    public List<RentalUnit> getMainRentalUnits() {
        return findByNamedQuery("getMainRentalUnits");
    }


    @Override
    public List<RentalUnit> getChildrenRentalUnitsById(long id) {
        return findByNamedQueryParameter("getChildrenRentalUnitsById","id",id);
    }

    @Override
    public RentalUnit getById(long id) {
        return findByNamedQueryParameter("getRentalUnitById","id",id).get(0);
    }

    @Override
    public List<RentalUnit> getAllRentalUnit() {
        return findByNamedQuery("getAllRentalUnit");
    }







}
