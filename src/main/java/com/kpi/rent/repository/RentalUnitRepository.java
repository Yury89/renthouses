package com.kpi.rent.repository;

import com.kpi.rent.entities.RentalUnit;

import java.util.List;

public interface RentalUnitRepository {

    List<RentalUnit> getMainRentalUnits();


    List<RentalUnit> getChildrenRentalUnitsById(long id);

    RentalUnit getById(long id);

    List<RentalUnit> getAllRentalUnit();





}
